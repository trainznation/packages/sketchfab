<?php

namespace Trainznation\Sketchfab;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Trainznation\Sketchfab\Skeleton\SkeletonClass
 */
class SketchfabFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'sketchfab';
    }
}
