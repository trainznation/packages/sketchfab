<?php

namespace Trainznation\Sketchfab;

use Illuminate\Support\Facades\Http;

class Sketchfab
{
    private $api_key;
    private $endpoint;

    protected $options = ["verify" => false];

    public function __construct()
    {
        $this->api_key = config('sketchfab.api_key');
        $this->endpoint = 'https://api.sketchfab.com/v3';
    }

    /**
     * @param $uri // Url d'appel
     * @return object|string
     */
    public function get($uri)
    {
        try {
            $client = Http::withOptions($this->options)->get($this->endpoint.$uri);
        }catch (\Exception $exception) {
            return $exception->getMessage();
        }

        return $client->object();
    }

    /**
     * Permet l'envoie d'un model 3D sur Sketchfab
     * @param $uri // URI D'appel
     * @param $filepath // Chemin du fichier à uploader
     * @return object|string|void
     */
    public function upload($uri, $filepath)
    {
        $ext = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
        if(!in_array($ext, config('sketchfab.supported_format'))) return;

        $filename = pathinfo($ext, PATHINFO_FILENAME);

        $params = [
            "name" => $filename,
            "modelFile" => new \CURLFile($filepath),
            "token" => $this->api_key,
            "private" => 0
        ];

        try {
            $client = Http::withOptions($this->options)->withToken($this->api_key)->post($uri, $params);
        }catch (\Exception $exception) {
            return $exception->getMessage();
        }

        return $client->object();
    }

}
